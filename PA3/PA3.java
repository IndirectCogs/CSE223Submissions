
 // December Redinger
 // CSE 223
 // Nick Macias
 // 5/3/2021

 // This is the main file for PA3. In PA3, we must create a binary tree with yes and no as the branches.
 // The initial ingestion occurs from a test file supplied by the problem - the results are saved for future games.
 // The questions go down the line, yes and no, until the final question: "Is it [blank]" and if the answer is wrong,
 // a new answer is supplied. 

 // Node Class pieces

 // String Data (The text given to the player.)
 // int Flag (Marking if the node is a question or answer.
 // Bnode Yes, No (The next Bnodes in the sequence.

 // The root is a question. Ask the question, get an answer. 

 // Travel to the left if yes, right if no.

 import java.util.Scanner;
 // Here import Printwriter and File


 public class PA3{

 // Read from a file. The first ingestion is the hardest one - the rest will be easier.

 // It seems as though there will be a for-loop that executes until a Flag returns with an answer marker.
 // It still doesn't really make sense how these Binary Nodes will knit together without Left and Right nodes.
 
 // A tree of Bnode's will be initialized by importing a file.


 // Put methods inside classes outside of main.


 public static void main(String[] args) {

 int count = args.length;

 File f = new File("20Q.txt");

 Scanner Choice = new Scanner(System.in);
 Scanner FileIO = new Scanner(f);

 Tree t = new Tree();
 t.Root = new Bnode();
 
 t.Root = Ingest(FileIO);

 game(t.Root, Choice);


 }

 // Ingest method
 
 Bnode Ingest(Scanner fc){
 
 String str1, str2;
 Scanner FileIO = new Scanner(f);
 
 // Read two lines.
 str1 = FileIO.nextLine();
 str2 = FileIO.nextLine();
 
 if (str1.equals("A:")){
	// Create a Bnode Answer and return it	
	Bnode Answer = new Bnode();
	
	Answer.Data = str1;
	Answer.Data2 = str2;
	Answer.Data3 = str1 + str2;
	return Answer;

	}

 if (str1.equals("Q:")){

	// make a node with that question
	Bnode Question = new Bnode();
	Question.Data = str1;
	Question.Data2 = str2;
	Question.Data3 = str1 + str2 ;
	Question.Yes = ingest(fc);
	Question.No = ingest(fc);
	return Question;
	
	}
 
 }

 public static void game(Bnode Root, Scanner sc){
	
	String IN = "", IN2 = "";
	Bnode temp = new Bnode();
	Bnode rootCopy = new Bnode();
	rootCopy = Root;	
	int prevDir = -1;
	PrintWriter writer = new PrintWriter(f);
	// While Root.Flag is 0
	while (Root.Flag == 0){

		temp = Root;	
		// Show Data

		System.out.println(Root.Data);
		// Get user input
		IN = sc.nextLine();
	
		// If IN is yes, root becomes root Yes. If no, Root No.
		if (IN.equals("Yes")){
			
			Root = Root.Yes;
			prevDir = 0;

			}
		if (IN.equals("No")){
			Root = Root.No;
			prevDir = 1;

			}
 	
 	}
		
	// Here, Flag will equal 1.
	System.out.println(Root.Data);
	
	// Get user input
	IN = sc.nextLine();

	if (IN.equals("Yes")){
		System.out.println("Aha! I knew it. Maybe next time you'll stump me?");
		writing(writer, rootCopy);
		return;

		}
	
	if (IN.equals("No")){
		
		// No protocol is much more complicated than yes.
		System.out.println("Oh no! What were you guessing? (Ask it as a question. 'Is it a [blank])");
		// Get input of the new guess.
		IN = sc.nextLine();
		System.out.println("Please enter a yes or no question where yes is your object choice, and no is mine.");
		// Get input for new question
		IN2 = sc.nextLine();
		
		Bnode new1 = new Bnode();
		Bnode new2 = new Bnode();
		
		new1.Data = IN;
		new1.Flag = 1;
		new2.Data = IN2;
		new2.Flag = 0;

		if (prevDir == 0)
			{
			temp.Yes = new2;

			}
		if (prevDire == 1)
			{
			temp.No = new2;
			}
		new2.Yes = new1;
		new2.No = Root;
		writing(writer, rootCopy);
		return;

		}
		}
 
 void writing(Printwriter w,Bnode root){
	
	// Write the root to the printwriter.
 	w.write(root);
	// Write yes and no. This is recursive, and will spread to the whole tree.
	writing(w, root.Yes);
	writing(w, root.No);

 	}	
 }
 

 // Binary Node definition

 class Bnode{

 // String is the question/answer

 String Data, Data2, Data3;

 // Yes is left, No is right

 Bnode Yes=null, No=null;

 // Flag is marking whether or not is question/answer (0 or 1)

 // Flag is set to 0, means answers should be set to 1
 int Flag=0;

 // getData will return the data in Data

 String getData(){
	
	return Data;

 }

 // getFlag will return the status of the flag. (possibly less important)

 int getFlag(){

	return Flag;

 }
 }

 class Tree{

 // This is the tree where Bnodes are stored

 Bnode Root = null;
 
 

 }
