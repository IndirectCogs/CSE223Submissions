 
 // December Redinger
 // CSE 223
 // Nick Macias
 // 4/19/2021

 // Indexer.java defines the class Indexer, which is meant to read a text file
 // and index the location of every word within the file.


 import java.util.LinkedList;
 import java.util.Scanner;
 import java.io.*;

 public class Indexer{
 
 // Public methods

 public boolean processFile(String filename){

 	// Input A string of the file name.
 	// Process open the filename read its contents, and build an index
	// First step - create a basic processor (read a file word by word.


	// Defining the File

	File file = new File(filename);
	Scanner input;
	try{
	input = new Scanner(file);
	}
	catch(Exception e){
	return false;
	}

	

	// Going word by word through the file
        while (input.hasNext()){
                String temp = input.next();
		// At this point, temp is a word. We can use temp in any of the methods that require a String word
		// We should clean up the word, then add a reference to it.
		// cleanup here
		temp = cleanupWord(temp);		
		// add reference here

		// debug print
                System.out.println(temp);
                }
		// Add reference goes here, save word and count
 	// Return
 return true;
 
 }
 /* 
 public int numberOfInstances(String word){
 
	// Input
	// Process
	// Return
 
 }

 public int locationOf(String word, int instanceNum){

	// Input
	// Process
	// Return

 }

 public int numberOfWords(){
	
	// Input
	// Process
	// Return

 }

 public String toString(){

	// Input
	// Process
	// Return

 }

 // Private Methods

 */
 private String cleanupWord(String word){

	// Process make every letter upper case, remove symbols
	word = word.replaceAll("[^a-zA-Z]", " ");
	word = word.toUpperCase(); 
	return word;

 }
 
 
 private void addReference(String word, int location){

	// Input A string word goes here, and an integer for the location.
	// Process
	// Return

 }
 }
