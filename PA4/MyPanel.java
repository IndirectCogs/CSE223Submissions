import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class MyPanel extends JPanel{

	// 2 dimensional array of boxes
	Box[][] boardArea = new Box[8][8];

	
	public MyPanel() {
		
		super();
		
		
		// Initialize boardArea here
		


		for (int i = 0; i < 8;i++) {
				for (int j=0; j < 8; j++) {
					// construct the board as boxes
					boardArea[i][j] = new Box();
					}
				}

		}

	// Increment allows us to create more or less dots as needed.
	public int[] xdata1 = new int[256];
	public int[] ydata1 = new int[256];
	public int[] xdata2 = new int[256];
	public int[] ydata2 = new int[256];
	public int xyloc=0;
	
	public int[] xloc = new int[100];
	public int[] yloc = new int[100];
	public char[] character = new char[100];
	public int placeholder=0;
	
	public int inc=50,a,b,c,d;
  
	// Paint method
	public void paint(Graphics g) {

		// use super classes here
		super.paint(g); // Do original JPanel's paint stuff.
		// Create a dot grid using pre-determined x and y stop locations.
		for (int x=50;x<500;x+=inc) {
			for (int y=50;y<500;y+=inc) {

				g.fillOval(x, y, 10, 10);

				
				}
			}
			for (int i = 0; i < 256; i++) {
				Graphics2D g2 = (Graphics2D) g;
				g2.setStroke(new BasicStroke(5));
				g2.drawLine(xdata1[i]+5, ydata1[i]+5, xdata2[i]+5,ydata2[i]+5);
				}
			
			for (int i =0; i < 100; i++) {
				
				g.drawString(String.valueOf(character[i]), xloc[i], yloc[i]);
			}
			
		
		

		}
	
	
	public void coordGrabber(int x1,int y1,int x2,int y2) {
		
		// define variables
		a = x1;
		b = y1;
		c = x2;
		d = y2;
		
		xdata1[xyloc] = a;
		ydata1[xyloc]= b;
		xdata2[xyloc]= c;
		ydata2[xyloc]= d;
		xyloc++;
		if (xyloc > 255) xyloc = 0;
		
		}
	
	public void initialDrawer(int a, int b, char c) {
		
		a = a*50+75;
		b = b*50+75;
		xloc[placeholder] = a;
		yloc[placeholder] = b;
		
		character[placeholder] = c;
		placeholder++;
		
	}



	}

