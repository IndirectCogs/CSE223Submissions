
// This is a functioning prototype of Dots.java , a program we were asked to build in PA4, and one we need to modify to act as a server/client connection in PA5.
// I took the time to rewrite and clean this up so that PA5 goes smoother.

// - December Redinger

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Dots extends JFrame {

	private JPanel contentPane;
	private JTextField nameFieldOne;
	private JTextField nameFieldTwo;
	Boolean playerOneTurn;
	int endFlag;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		setTitle("Dots.java");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel playerOneName = new JLabel("Change Player One Name Here");
		playerOneName.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerOneName.setForeground(Color.ORANGE);
		playerOneName.setBounds(525, 115, 215, 15);
		contentPane.add(playerOneName);
		
		JLabel playerTwoName = new JLabel("Change Player Two Name Here");
		playerTwoName.setForeground(Color.ORANGE);
		playerTwoName.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerTwoName.setBounds(525, 200, 215, 15);
		contentPane.add(playerTwoName);
		
		JButton btnNewButton = new JButton("RESET");
		btnNewButton.setBounds(545, 425, 100, 25);
		contentPane.add(btnNewButton);
		
		GameData DBGame = new GameData();
		MyPanel myBoard = new MyPanel();
		
		myBoard.setBounds(10, 45, 500, 500);
		contentPane.add(myBoard);
		
		JLabel playerOneLabel = new JLabel("Player One Score: ");
		playerOneLabel.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerOneLabel.setForeground(Color.ORANGE);
		playerOneLabel.setBounds(10, 15, 125, 15);
		contentPane.add(playerOneLabel);
		
		JLabel playerTwoLabel = new JLabel("Player Two Score: ");
		playerTwoLabel.setForeground(Color.ORANGE);
		playerTwoLabel.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerTwoLabel.setBounds(200, 15, 125, 15);
		contentPane.add(playerTwoLabel);
		
		JLabel playerOneScore = new JLabel("00");
		playerOneScore.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerOneScore.setForeground(Color.ORANGE);
		playerOneScore.setBounds(150, 15, 30, 15);
		contentPane.add(playerOneScore);
		
		JLabel playerTwoScore = new JLabel("00");
		playerTwoScore.setForeground(Color.ORANGE);
		playerTwoScore.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		playerTwoScore.setBounds(325, 15, 30, 15);
		contentPane.add(playerTwoScore);
		
		JLabel turnLabel = new JLabel("Player One's Turn!");
		turnLabel.setFont(new Font("Segoe UI Symbol", Font.BOLD, 14));
		turnLabel.setForeground(Color.ORANGE);
		turnLabel.setBounds(379, 15, 131, 14);
		contentPane.add(turnLabel);
		
		JLabel Title = new JLabel("Dots and Boxes");
		Title.setFont(new Font("Segoe UI Symbol", Font.BOLD | Font.ITALIC, 20));
		Title.setForeground(Color.ORANGE);
		Title.setBounds(545, 15, 159, 37);
		contentPane.add(Title);
		
		playerOneTurn = true;
		endFlag = 0;
		myBoard.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if (endFlag == 0) {
				Boolean check = false;
				Boolean check2 = false;
				
				int tempFlag = 0;
				
				
				// While turn does not equal 1, play game.
				

					// check

					// Draw Line part of turn 
					// Get x and y
					int x = e.getX();
					int y = e.getY();
					// Defining cell width and height.
					int cellW=50,cellH=50;
					int rowClicked, columnClicked;
					
					// Defining which row and column was clicked.
					// 50 is subtracted from x and y because the first dot is at (50,50)
					
					rowClicked = (y-50)/cellH;
					columnClicked= (x-50)/cellW;
					String whichLine = "";

					// Find closest line
					int x1 = columnClicked * cellW + 50 , x2 = (columnClicked+1)*cellW + 50 ;
					int y1 = rowClicked * cellH + 50, y2 = (rowClicked+1)*cellH + 50 ;
					int leftLine,rightLine,topLine,bottomLine;

					// Define distances
					leftLine = x - x1;
					rightLine = x2 - x;
					topLine = y - y1;
					bottomLine = y2 - y;

					// Do 4 checks for left, right, up, down
					if ((leftLine < rightLine) && (leftLine < topLine) && (leftLine < bottomLine)) {
						whichLine = "left";

					}
					else if ((rightLine < leftLine) && (rightLine < topLine) && (rightLine < bottomLine)) {
						whichLine = "right";
					}
					else if ((topLine < leftLine) && (topLine < rightLine) && (topLine < bottomLine)) {
						whichLine = "top";
					}
					else {
						whichLine = "bottom";
					}

					// comparing strings (to edit boxes.)
					// (x1,y1) , (x2,y1) , (x1,y2) , (x2, y2)
					
					
					// by this point the closest line is found
					
					if (whichLine.equals("left")) {
						myBoard.boardArea[columnClicked][rowClicked].left = true;
						if (columnClicked != 0) {
							myBoard.boardArea[columnClicked-1][rowClicked].right = true;
						}
						
						
						
						// coord grabber draws lines
						myBoard.coordGrabber(x1, y1, x1, y2);
						repaint();
						check = myBoard.boardArea[columnClicked][rowClicked].completeCheck();
						if (columnClicked != 0) {
							check2 = myBoard.boardArea[columnClicked-1][rowClicked].completeCheck();
						}
						
						if (check == true) {
							
							if (playerOneTurn == true) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
							if (playerOneTurn == false) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
						}
						
						if (columnClicked != 0) {
						if (check2 == true){
							
							if (playerOneTurn == true) {
								myBoard.boardArea[columnClicked-1][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked-1,rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
							if (playerOneTurn == false) {
								
								myBoard.boardArea[columnClicked-1][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked-1,rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								repaint();
								
							}
							
						}
					 }}

						if (whichLine.equals("right")) {
						myBoard.boardArea[columnClicked][rowClicked].right = true;
						if (columnClicked != 7) {
							myBoard.boardArea[columnClicked+1][rowClicked].left = true;
						}
						
						
						myBoard.coordGrabber(x2, y1, x2, y2);
						repaint();
						check = myBoard.boardArea[columnClicked][rowClicked].completeCheck();
						if (columnClicked != 7) {
							check2 = myBoard.boardArea[columnClicked+1][rowClicked].completeCheck();
						}
						
						if (check == true) {
							
							if (playerOneTurn == true) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
							if (playerOneTurn == false) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
						}
						
						if (columnClicked != 7) {
						if (check2 == true) {
							
							if (playerOneTurn == true) {
								myBoard.boardArea[columnClicked+1][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked+1, rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
								
							}
							if (playerOneTurn == false) {
								myBoard.boardArea[columnClicked+1][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked+1, rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
						}
					}}
					

						 if (whichLine.equals("top")) {
						// Do this fix for all integer overflows or underflows and for overflows say if clicked < 7
						// do this for rewrite
						
						if (rowClicked != 0 ) { 
							
							
							myBoard.boardArea[columnClicked][rowClicked-1].down = true;
							
						}
						
						myBoard.boardArea[columnClicked][rowClicked].up = true;
						myBoard.coordGrabber(x1, y1, x2, y1);
						repaint();
						
						// Something about this code seems particularly sloppy, having to input the box in both boardArea and completeCheck.
						// Perhaps just saying something like
						// check = completeCheck(Specific Box Here) ; ??
						check = myBoard.boardArea[columnClicked][rowClicked].completeCheck();
						
						
						
						// Fix the underflow here
						if (rowClicked != 0) {
						
							check2 = myBoard.boardArea[columnClicked][rowClicked-1].completeCheck();
							
						}
						
						if (check == true) {
							
							if (playerOneTurn == true) {
								
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
							if (playerOneTurn == false) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								
								repaint();
							}
						}
					
						if (rowClicked != 0) {
						if (check2 == true) {
							
							if(playerOneTurn == true) {
								myBoard.boardArea[columnClicked][rowClicked-1].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked, rowClicked-1, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
							}
							if(playerOneTurn == false) {
								myBoard.boardArea[columnClicked][rowClicked-1].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked-1, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								
								
							}
						}
						}}
						else if (whichLine.equals("bottom")) {
						myBoard.boardArea[columnClicked][rowClicked].down = true;
						
						// Check for overflow here
						if (rowClicked != 7) {
							myBoard.boardArea[columnClicked][rowClicked+1].up = true;
						}
						
						myBoard.coordGrabber(x1, y2, x2, y2);
						repaint();
						
						check = myBoard.boardArea[columnClicked][rowClicked].completeCheck();
						
						
						// Check for overflow here
						if (rowClicked != 7) {
							check2 = myBoard.boardArea[columnClicked][rowClicked+1].completeCheck();
							
						}
						
						if (check == true) {
							
							if (playerOneTurn == true) {
								
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialOne;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								repaint();
								
							}
							if (playerOneTurn == false) {
								myBoard.boardArea[columnClicked][rowClicked].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								
							}
						}
						if (rowClicked != 7) {
						if (check2 == true) {
							if (playerOneTurn == true) {
							
								myBoard.boardArea[columnClicked][rowClicked+1].initial = DBGame.initialOne;	
								myBoard.initialDrawer(columnClicked, rowClicked+1, DBGame.initialOne);
								DBGame.score1 = DBGame.score1 + 1;
								tempFlag = tempFlag + 1;
								
							}
							if (playerOneTurn == false) {
								
								myBoard.boardArea[columnClicked][rowClicked+1].initial = DBGame.initialTwo;
								myBoard.initialDrawer(columnClicked, rowClicked+1, DBGame.initialTwo);
								DBGame.score2 = DBGame.score2 + 1;
								tempFlag = tempFlag + 1;
								
							}
						}
						}
					}
					
					
				
					// End of turn}
					
					// Update Scoreboards
					playerOneScore.setText(""+DBGame.score1);
					playerTwoScore.setText(""+DBGame.score2);
					
					if (DBGame.score1 == 32) {
						
						playerOneLabel.setText("PLAYER ONE WINS!!!");
						playerTwoLabel.setText("PLAYER ONE WINS!!!");
						turnLabel.setText("GAME OVER!!!");
						endFlag = 1;
					}
					else if (DBGame.score2 == 32) {
						
						playerOneLabel.setText("PLAYER TWO WINS!!!");
						playerTwoLabel.setText("PLAYER TWO WINS!!!");
						turnLabel.setText("GAME OVER!!!");
						endFlag = 1;
					}
					
					// Update Turn playerOneTurn
					if (tempFlag == 0) {
						
							if (playerOneTurn == true) {
								playerOneTurn = false;
								turnLabel.setText("Player Two's Turn!");
								
							}
							
							else if (playerOneTurn == false) {
								playerOneTurn = true;
								turnLabel.setText("Player One's Turn!");
								
							}
						}
				}}}
		);

		
		nameFieldOne = new JTextField();

		nameFieldOne.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				
				nameFieldOne.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						
						int keyCode = e.getKeyCode();
						if (keyCode == 10) {
							String newName = nameFieldOne.getText();
							playerOneName.setText(newName);
							DBGame.initialOne = newName.charAt(0);
						}
						
					}
				});
				
			}
		});
		nameFieldOne.setBounds(545, 140, 125, 30);
		contentPane.add(nameFieldOne);
		nameFieldOne.setColumns(10);
		
		nameFieldTwo = new JTextField();
		nameFieldTwo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				nameFieldTwo.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						int keyCode = e.getKeyCode();
						if (keyCode == 10) {
							String newName = nameFieldTwo.getText();
							playerTwoName.setText(newName);
							DBGame.initialTwo = newName.charAt(0);
							
						}
						
					}
				});
				
			}
		});
		nameFieldTwo.setColumns(10);
		nameFieldTwo.setBounds(545, 226, 125, 30);
		contentPane.add(nameFieldTwo);
		
	}
}

