import java.io.PrintWriter;
import java.util.Scanner;
import java.net.Socket;

class MyThread extends Thread
{
  Socket sock;
  boolean toFlag;
  PrintWriter pw; // for writing to the socket
  Scanner sc; // for reading form the socket

  public MyThread(Socket sock,boolean toFlag)
  {
    this.sock=sock; // THE socket
    this.toFlag=toFlag;

    try{
      if (toFlag){
        pw=new PrintWriter(sock.getOutputStream());
      } else {
        sc=new Scanner(sock.getInputStream());
      }
    } catch (Exception e){
      System.out.println("Error: " + e);return;
    }
  }

  public void run() // run when start() is called
  {
// now do your thing
    if (toFlag){// read from stdin, send to pw
      Scanner stdin=new Scanner(System.in);
      while (stdin.hasNextLine()){
        pw.println(stdin.nextLine());
        pw.flush();
      }
      pw.close();
      return;
    }

// else toFlag is false...read from socket, send to stdout
    while (sc.hasNextLine()){
      System.out.println(sc.nextLine());
    }
    sc.close();
    return;
  }
}
