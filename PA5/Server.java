//Server.java

import java.net.ServerSocket;
import java.net.Socket;

class Server
{
  public static void main(String[] args)
  {
// create a server socket and wait for a connection
    ServerSocket ss;
    Socket sock;

    try{
      ss=new ServerSocket(1234);
    } catch (Exception e){
      System.out.println("Error: " + e);return;
    }

    try{
      sock=ss.accept(); // receive a connection
    } catch (Exception e){
      System.out.println("Error: " + e);return;
    }

// sock is now available
// make a pair of threads for communicating with the client
// MyThread(socket,toFlag)
    MyThread to=new MyThread(sock,true); // for sending TO the other side (client)
    MyThread from=new MyThread(sock,false); // for receiving FROM the other side (client)
    to.start();
    from.start();
    System.out.println("Main thread is done");
    return;
  }
}
