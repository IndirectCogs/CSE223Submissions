
 // December Redinger
 // CSE 223
 // Nick Macias
 // 4/13/2021

 // Fractions.java
 // For this programming assignment, we are asked to create a class implementing a new data type (fractions)
 // We are also asked to code methods that allow us to manipulate variables using this data type.

 
 class Fraction{

 	int num; // numerator
	int denom; // denominator
	// if there is only one argument, denominator is 1.
	
	Fraction(int A,int B)

	{
		// Constructor

		num = A;
		denom = B;
	
	}
	
	public Fraction add(Fraction n) // returns x + n in reduced form
	
	{
		// Input: A fraction n
		int newnum, newdenom;
			
		newnum = n.num + num;
		newdenom = n.denom + denom;

		Fraction result = new Fraction(newnum,newdenom);

		// Return: The simplified rational x+n
		return result;

	}

	public Fraction sub(Fraction n) // returns x - n in reduced form
	
	{
		int newnum,newdenom;

		newnum = n.num - num;
		newdenom = n.denom - num;

		Fraction result = new Fraction(newnum,newdenom);

		// Return: The simplified rational x - n
		return result;

	}

	public Fraction mul(Fraction n) // returns x * n in reduced form
	
	{
		// Input: A fraction n
		int newnum, newdenom;
	
		newnum = n.num * num;
		newdenom = n.denom * denom;

		Fraction result = new Fraction(newnum,newdenom);

		// Return the product of the fractions
	
		return result;

	}

	public Fraction div(Fraction n) // returns x / n in reduced form
	
	{
		// Input: A fraction n
		// Division is different in that to divide a fraction, you flip one fraction and multiply them together.
		int newnum,newdenom;

		newnum = n.num * denom;
		newdenom = n.denom * num;
	
		Fraction result = new Fraction(newnum,newdenom);

		// Return: The simplified rational x / n
	
		return result;

	}

	double toDouble() // returns x as a double-precision floating point number
	
	{
		// the double is numerator divided by denominator of the object

		double result = num / denom;

		return result;
	}

	int getNum() // Returns numerator

	{

		// get the numerator of the object
		int result = num;

		return result;
	}

	int getDenom() // Returns denominator
	{
		// get the denominator of the object
		int result = denom;

		return result;

	}
	String toString(Fraction n) // Return a string representing the fraction.

	{
		// return a string reperesenting the fraction.

		String result = num + "/" + denom;
		return result;
	}

	// Optional, code this if you finish project early or if it becomes neccessary. private x.reduce() // Adjusts num and denom so num/denom is in simplest form


 }
